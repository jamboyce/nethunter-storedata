Opaque is a GPLv3 licensed secure, super fast oVirt, Red Hat Enterprise Virtualization (RHEV), and Proxmox VDI client that allows you to connect graphically to all your virtual machines running in the cloud from anywhere in the world. It currently implements support for the SPICE protocol.

NOTES
(1) Some older devices are known to have issues with self-signed SSL certificates. If you find you are affected, return Opaque or ask for a refund. Please do not rate the application badly because of a platform issue!

(2) On most current devices, playing sound and video exceeds the CPU processing power and causes jittery sound and video playback.

(3) If virtual machines are behind a RHEV/oVirt proxy, you may need to connect to them with Opaque by using a .vv file. To get a .vv file, you have to login through the oVirt/RHEV web interface and connect to your VM's console there. Chrome works perfectly with the web interface, and opens Opaque directly. With Firefox, you have to open a File Manager, and tap the file in the Downloads folder as soon as the download completes. The built-in Android Browser does not work for .vv downloading.
* System administrators: If you are planning to recommend this app, please ensure you have determined the simplest method of access for your environment.
* Users: If you're unsure, please ask your System Administrator!

(4) The oVirt web "User Portal" for oVirt v3.4 and older does not allow obtaining .vv files for virtual machines.

(5) Direct connections to Proxmox VMs are now fully supported. Select Proxmox from the Virtualization Environment drop-down when adding a connection (tap the "+" at the main screen).

Current features include:
- Full Proxmox and oVirt/RHEV integration.
- TFA / OTP Two factor authentication with Proxmox (Yubikey, Google Authenticator, etc.)
- Android 4.x and earlier: USB redirection with OTG enabled devices. Connect your OTG cable prior to connecting to your VM. For some Android 4.x and Android 5.0 and later, your device must be rooted and you have to use "SELinux Mode Changer" (from Google Play) to set selinux to permissive mode.
- Audio playback/recording
- Supports both Proxmox and oVirt/RHEV virt-viewer/remote-viewer (.vv) files
- Ability to edit default settings for .vv file connections or to make creation of new connections more convenient (on the main app page, tap the wrench at the top-right corner or look in the menu on older devices).
- SSL encryption for peace of mind
- Commercially signed and self-signed certificates
- SPICE protocol support
- Automatic rotation
- Desktop resizing to match display resolution
- Pinch-zoom
- Multi-touch UI with four mouse input modes to choose from
- Mouse scrolling by swiping with two fingers up/down
- Single-handed mode for easy on-the-go operation
- Support for most bluetooth input devices.

Planned features:
- Allocating VMs from a pool
- Clipboard integration
- VNC protocol support

Opaque source code is GPLv3 licensed, and is available here:
https://github.com/iiordanov/remote-desktop-clients

It also uses a number of open-source libraries included in the Gstreamer SDK for Android, as well as libcelt, openssl, libgovirt, jpeg-turbo, and librest.

This app is built and signed by Kali NetHunter.
